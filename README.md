# Public transportation


This an api for tracking buses, creating, updating and deleting routes, bus stops, distance beetwen it and timetable for all of that.

The used technologies:

  - python 3.6.7
  - Django 2.1.7
  - Djangorestframework 3.9.2

### Installation

First of all create an virtual environment

```sh
$ pip3 install virtualenv
$ virtualenv venv
$ source venv/bin/activate
```

Clone the repo to some directory

```sh
$ git clone https://van2102@bitbucket.org/van2102/public_transportation.git
```

Install requirements

```sh
$ pip3 install > requirements.txt
```
Start app

```sh
$ ./manage.py runserver
```

To run testing try

```sh
$ ./manage.py test
```

# Create test database
Command below delete all non-django instances from database and fill it with test data
```sh
$ ./manage.py create_test_data
```

# Usage

### Routes
