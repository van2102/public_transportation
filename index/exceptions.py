import datetime
import logging
from rest_framework.views import Response, status
logger = logging.getLogger(__name__)


def value_error_exception_handler(exc, context):
    error = type(exc).__name__
    status_code = status.HTTP_400_BAD_REQUEST
    if error == 'MethodNotAllowed':
        status_code = status.HTTP_405_METHOD_NOT_ALLOWED
    generated_exc_message = repr(exc)
    error_dict = {'ErrorObject': {
        'ErrorName': error,
        'ErrorClassDesription': repr(exc),
        'Time': datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S"), },
        'ProblemObject': context}
    logger.error(error_dict)
    return Response({'Error': generated_exc_message},
                    status=status_code)
