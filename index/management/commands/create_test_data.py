from django.core.management.base import BaseCommand
import csv

from index import models


class Command(BaseCommand):
    help = 'Delete all data from database and fill it with test data'

    def handle(self, *args, **options):
        self.stops = []
        self.routes = []
        self.bus_route_items = []
        self.timetable_items = []
        self.connection_items = []

        models.Connection.objects.all().delete()
        models.TimetableItem.objects.all().delete()
        models.BusRouteItem.objects.all().delete()
        models.BusStop.objects.all().delete()
        models.Route.objects.all().delete()

        arrival_times = self.parse_csv('data/time.csv')
        route_names = self.parse_csv('data/routes.csv')
        distances = self.parse_csv('data/distances.csv')
        stop_names = self.parse_csv('data/stops.csv')

        for name in stop_names:
            self.stops.append(
                models.BusStop.objects.create(name=''.join(name)))

        for name in route_names:
            self.routes.append(models.Route.objects.create(name=''.join(name)))

        for i in range(len(self.stops)):
            self.connection_items.append(models.Connection.objects.create(
                distance_length=''.join(distances[i]),
                bus_stop=self.stops[i],
                next_bus_stop=self.stops[i +
                                         1] if i < len(self.stops)-1 else None
            ))

        for i in reversed(range(len(self.stops))):
            self.connection_items.append(models.Connection.objects.create(
                distance_length=''.join(distances[i]),
                bus_stop=self.stops[i],
                next_bus_stop=self.stops[i -
                                         1] if i > 0 else None
            ))

        for i in reversed(range(len(self.stops))):
            self.connection_items.append(models.Connection.objects.create(
                distance_length=''.join(distances[i]),
                bus_stop=self.stops[i],
                next_bus_stop=self.stops[i -
                                         1] if i > len(self.stops)+1 else None
            ))

        for i in range(len(self.stops)):
            self.bus_route_items.append(
                models.BusRouteItem.objects.create(
                    route=self.routes[0], connection=self.connection_items[i]
                )
            )

        for i in range(len(self.stops)):
            self.timetable_items.append(
                models.TimetableItem.objects.create(
                    route_item=self.bus_route_items[i],
                    arrival_time=''.join(arrival_times[i]),
                    bus_index=1)
            )

        self.stdout.write('Test data was added successful')

    @staticmethod
    def parse_csv(path_to_file):
        with open(path_to_file, 'r') as f:
            reader = csv.reader(f)
            data_list = list(reader)
        return data_list
