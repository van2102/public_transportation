from django.db import models
import pytest


pytestmark = pytest.mark.webtest


class Route(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'route'


class BusStop(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'bus_stop'


class Connection(models.Model):
    distance_length = models.FloatField(null=True)
    bus_stop = models.ForeignKey(
        BusStop, db_index=True, null=True, on_delete=models.CASCADE)
    next_bus_stop = models.ForeignKey(
        BusStop, db_index=True, null=True, related_name='next_bus_stop', on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'connection'


class BusRouteItem(models.Model):
    route = models.ForeignKey(
        Route, db_index=True, null=True, on_delete=models.CASCADE)
    connection = models.ForeignKey(
        Connection, db_index=True, null=True, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'bus_route_item'


class TimetableItem(models.Model):
    route_item = models.ForeignKey(
        BusRouteItem, null=True, on_delete=models.CASCADE)
    arrival_time = models.TimeField(null=True)
    bus_index = models.IntegerField(null=True)

    class Meta:
        managed = True
        db_table = 'timetable_item'
