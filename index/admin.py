from django.contrib import admin
from .models import BusRouteItem, BusStop, Connection, Route, TimetableItem

admin.site.register(BusRouteItem)
admin.site.register(BusStop)
admin.site.register(Connection)
admin.site.register(Route)
admin.site.register(TimetableItem)
# Register your models here.
