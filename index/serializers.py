from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist

from .models import BusRouteItem, BusStop, Connection, Route


class BusStopSerializer(serializers.ModelSerializer):
    """
    The serializer for BusStop model to using .create(), .update(), .destroy(), .list() methods of ModelViewSet
    """

    name = serializers.CharField(
        required=True, help_text='Name of bus stop')

    class Meta:
        model = BusStop
        fields = ('id', 'name')
        ref_name = 'BusStop'


class BusStopReadOnlySerializer(serializers.ModelSerializer):
    """
    The serializer for BusStop model for ModelViewSet fields representation
    """

    name = serializers.CharField(
        required=False, read_only=True, help_text='Name of bus stop')
    id = serializers.IntegerField(required=False, read_only=True,)

    class Meta:
        model = BusStop
        fields = ('id', 'name')
        ref_name = None


class RouteSerializer(serializers.ModelSerializer):
    """
    The serializer for Route model to using .create(), .update(), .destroy(), .list() methods of ModelViewSet
    """

    name = serializers.CharField(
        required=True, help_text='Name of route')

    class Meta:
        model = Route
        fields = ('id', 'name')
        ref_name = 'Route'


class RouteReadOnlySerializer(serializers.ModelSerializer):
    """
    The serializer for Route model for ModelViewSet fields representation
    """

    name = serializers.CharField(
        required=False, read_only=True, help_text='Name of bus stop')
    id = serializers.IntegerField(required=False, read_only=True,)

    class Meta:
        model = Route
        fields = ('id', 'name')
        ref_name = None


class ConnectionSerializer(serializers.ModelSerializer):
    """
    The serializer for Connection model to using .create(), .update(), .destroy(), .list() methods of ModelViewSet
    """

    bus_stop = BusStopReadOnlySerializer(
        read_only=True, help_text='The BusStop object of current stop.')
    next_bus_stop = BusStopReadOnlySerializer(
        read_only=True, help_text='The BusStop object of next stop.')
    bus_stop_id = serializers.IntegerField(
        write_only=True, required=True, help_text='The id of BusStop object of current stop.')
    next_bus_stop_id = serializers.IntegerField(
        write_only=True, help_text='The id of BusStop object of next stop.')
    distance_length = serializers.FloatField(
        required=True, help_text='The distance between stops [km]')

    class Meta:
        model = Connection
        fields = ('id', 'distance_length', 'bus_stop',
                  'next_bus_stop', 'bus_stop_id', 'next_bus_stop_id')
        ref_name = 'Connection'

    def create(self, validated_data):
        bus_stop_instance = BusStop.objects.get(
            id=validated_data['bus_stop_id'])
        next_bus_stop_instance = BusStop.objects.get(
            id=validated_data['next_bus_stop_id'])
        new_instance = Connection.objects.create(distance_length=validated_data['distance_length'],
                                                 bus_stop=bus_stop_instance, next_bus_stop=next_bus_stop_instance)
        new_instance.save()

        return new_instance

    def update(self, instance, validated_data):
        try:
            bus_stop_instance = BusStop.objects.get(
                id=validated_data['bus_stop_id'])
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist(
                f'Connection item with id {validated_data["bus_stop_id"]} does not exist')
        next_bus_stop_instance = BusStop.objects.get(
            id=validated_data['next_bus_stop_id'])
        instance.distance_length = validated_data['distance_length']
        instance.bus_stop = bus_stop_instance
        instance.next_bus_stop = next_bus_stop_instance
        instance.save()

        return instance


class ConnectionReadOnlySerializer(serializers.ModelSerializer):
    """
    The serializer for Connection model for ModelViewSet fields representation
    """

    bus_stop = BusStopSerializer(
        read_only=True, help_text='The BusStop object of current stop.')
    next_bus_stop = BusStopSerializer(
        read_only=True, help_text='The BusStop object of next stop.')
    id = serializers.IntegerField(required=False, read_only=True,)
    distance_length = serializers.FloatField(
        read_only=True, help_text='The distance between stops [km]')

    class Meta:
        model = Connection
        fields = ('id', 'distance_length', 'bus_stop',
                  'next_bus_stop')
        ref_name = None


class BusRouteItemCreateUpdateDeleteSerializer(serializers.ModelSerializer):
    connection_id = serializers.IntegerField(write_only=True)
    route_id = serializers.IntegerField(write_only=True)
    connection = ConnectionReadOnlySerializer(read_only=True)
    route = RouteReadOnlySerializer(read_only=True)

    class Meta:
        model = BusRouteItem
        fields = ('id', 'connection', 'route', 'connection_id', 'route_id')

    def create(self, validated_data):
        route_instance = Route.objects.get(id=validated_data['route_id'])
        connection_instance = Connection.objects.get(
            id=validated_data['connection_id'])
        busrouteitem_instance = BusRouteItem.objects.create(
            route=route_instance, connection=connection_instance)

        return busrouteitem_instance

    def update(self, instance, validated_data):
        busrouteitem_instance = instance
        route_instance = Route.objects.get(id=validated_data['route_id'])
        connection_instance = Connection.objects.get(
            id=validated_data['connection_id'])
        busrouteitem_instance.route = route_instance
        busrouteitem_instance.connection = connection_instance
        busrouteitem_instance.save()

        return busrouteitem_instance

    def destroy(self, instance):
        busrouteitem_instance = instance
        busrouteitem_id = busrouteitem_instance.id
        busrouteitem_instance.delete()

        return {'id': busrouteitem_id}


class BusRouteItemSerializer(serializers.ModelSerializer):
    """
    The serializer for BusRoute model to using .create(), .update(), .destroy(), .list() methods of ModelViewSet
    """

    connection = ConnectionReadOnlySerializer(
        read_only=True, help_text='The Connection object')
    connection_id = serializers.IntegerField(
        write_only=True, required=True, help_text='The id of Connection object')
    id = serializers.IntegerField(write_only=True, required=False,
                                  help_text='The id of BusRouteItem. It is needed when the model to update will save this BusRouteItem. For create it doesn\'t needed.')

    class Meta:
        model = BusRouteItem
        fields = ('id', 'connection', 'connection_id')
        ref_name = 'BusRouteitem'


class BusRouteItemReadOnlySerializer(serializers.ModelSerializer):
    """
    The serializer for BusRouteItem model for ModelViewSet fields representation
    """

    connection = ConnectionReadOnlySerializer(
        read_only=True, help_text='The Connection object')

    class Meta:
        model = BusRouteItem
        fields = ('id', 'connection')
        ref_name = None


class RouteAndItemsSerializer(serializers.ModelSerializer):
    """
    The serializer for Route resource ViewSet methods
    """

    busrouteitem_set = BusRouteItemReadOnlySerializer(
        many=True, read_only=True)
    busrouteitem_id_set = BusRouteItemSerializer(many=True, write_only=True,
                                                 help_text='BusRouteItem objects list.'
                                                 'To create a new route it\'s need to give a connections '
                                                 'id query in a right order.'
                                                 ' To update it needs the similar actions but id doesn\'t required.'
                                                 )

    class Meta:
        model = Route
        fields = ('id', 'name', 'busrouteitem_set', 'busrouteitem_id_set')

    def sort_busrouteitems(self, busrouteitem_set):
        init_item = None
        ordered_query = []
        for item in busrouteitem_set:
            is_init = True
            for _item in busrouteitem_set:
                if item.connection.bus_stop == _item.connection.next_bus_stop:
                    is_init = False
            if is_init:
                init_item = item

        for item in busrouteitem_set:
            ordered_query.append(init_item)
            if not init_item.connection.next_bus_stop:
                break
            for _item in busrouteitem_set:
                if init_item.connection.next_bus_stop == _item.connection.bus_stop:
                    init_item = _item
                    break
        return ordered_query

    def to_representation(self, obj):
        bus_route_items_query = self.sort_busrouteitems(
            obj.busrouteitem_set.all())
        return {'id': obj.id,
                'name': obj.name,
                'busrouteitem_set': BusRouteItemReadOnlySerializer(bus_route_items_query, many=True).data}

    def validate_name(self, value):
        if len(value.split()) > 1:
            raise serializers.ValidationError("Name must be only one word")
        return value

    def validate_busrouteitem_id_set(self, value):
        connection_len = len(value) - 1
        for count, id_item in enumerate(value):
            connection_instance = Connection.objects.get(
                id=value[count]['connection_id'])
            next_connection_instance = None
            if count < connection_len:
                next_connection_instance = Connection.objects.get(
                    id=value[count+1]['connection_id'])
            if next_connection_instance:
                if not connection_instance.next_bus_stop == next_connection_instance.bus_stop:
                    raise serializers.ValidationError(
                        'Connections must be orderded correctly (next stop of connection must be a current stop of next connection)')
            else:
                if connection_instance.next_bus_stop:
                    raise serializers.ValidationError(
                        'Last stop must be nullable instance ("null")')
        return value

    def create(self, validated_data):
        route_name = validated_data['name']
        route_instance = RouteSerializer().create({'name': route_name})
        connection_item_id_set = validated_data['busrouteitem_id_set']
        for id_item in connection_item_id_set:
            route_item = BusRouteItemCreateUpdateDeleteSerializer().create(
                {'route_id': route_instance.id, 'connection_id': id_item['connection_id']})

        return route_instance

    def update(self, instance, validated_data):
        instance.name = validated_data['name']
        instance.save()

        routeitems_id_list = [
            (id_list.connection.id, id_list.id)
            for id_list
            in instance.busrouteitem_set.all()
        ]
        new_routeitems_id_list = [
            (id_list['connection_id'], id_list.get('id'))
            for id_list
            in validated_data['busrouteitem_id_set']
        ]

        for routeitem in routeitems_id_list.copy():
            if routeitem not in new_routeitems_id_list:
                routeitem_instance = BusRouteItem.objects.get(id=routeitem[1])
                routeitem_instance.delete()
                routeitems_id_list.remove(routeitem)

        for routeitem in new_routeitems_id_list.copy():
            if routeitem not in routeitems_id_list:
                connection_instance = Connection.objects.get(id=routeitem[0])
                new_routeitem = BusRouteItem.objects.create(
                    route=instance, connection=connection_instance)

        return instance

    def destroy(self, instance):
        route_id = instance.id
        busrouteitem_set = instance.busrouteitem_set
        for busrouteitem in busrouteitem_set:
            busrouteitem.delete()

        instance.delete()

        return {'id': route_id}
