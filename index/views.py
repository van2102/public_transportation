from .models import Connection, Route
from .serializers import RouteAndItemsSerializer, ConnectionReadOnlySerializer
from rest_framework.viewsets import ModelViewSet


class RoutesViewSet(ModelViewSet):
    """
    Represent a Route resource to get, create, update and delete whole resource including BusRouteItems model.
    """
    queryset = Route.objects.all()
    serializer_class = RouteAndItemsSerializer
    http_method_names = ['get', 'post', 'put', 'delete']


class ConnectionViewSet(ModelViewSet):
    """
    ViewSet to get a connection objects that needed to Route work on.
    """

    queryset = Connection.objects.all()
    serializer_class = ConnectionReadOnlySerializer
    http_method_names = ['get']
