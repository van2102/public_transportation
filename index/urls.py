from django.urls import path, include
from .views import RoutesViewSet, ConnectionViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'route', RoutesViewSet)
router.register(r'connection', ConnectionViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
