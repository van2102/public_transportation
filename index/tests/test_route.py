import pytest
from django.test import Client
from itertools import zip_longest

from index.models import BusRouteItem, BusStop, Connection, Route
from .factories import RouteFactory, BusStopFactory, ConnectionFactory, BusRouteItemFactory


pytestmark = pytest.mark.django_db


@pytest.fixture
def create_data_samples(request):
    """
    Create a test data to test the Route resource. It creates a data sample for every put test case.
    """

    stops_len = 22
    stops = [BusStopFactory() for x in range(stops_len)]
    next_stop = None
    connections = []
    route_cases = []
    route_item_cases = []

    for count in range(0, 7):
        stop = stops[count]
        if count < 6:
            next_stop = stops[count+1]
        else:
            next_stop = None
        connection = ConnectionFactory(
            bus_stop=stop, next_bus_stop=next_stop)
        connections.append(connection)

    def create_route_items(route_case):
        route_items_list = []
        for count in range(0, 7):
            route_item = BusRouteItemFactory(
                route=route_case, connection=connections[count])
            route_items_list.append(route_item)
        return route_items_list

    route_cases.append(RouteFactory())
    route_cases.append(RouteFactory())
    route_cases.append(RouteFactory())
    route_cases.append(RouteFactory())
    route_cases.append(RouteFactory())
    route_item_cases.append(create_route_items(route_cases[0]))
    route_item_cases.append(create_route_items(route_cases[1]))
    route_item_cases.append(create_route_items(route_cases[2]))
    route_item_cases.append(create_route_items(route_cases[3]))
    route_item_cases.append(create_route_items(route_cases[4]))

    connections.append(ConnectionFactory(
        bus_stop=stops[4], next_bus_stop=None))
    connections.append(ConnectionFactory(
        bus_stop=stops[2], next_bus_stop=stops[4]))
    connections.append(ConnectionFactory(
        bus_stop=stops[5], next_bus_stop=stops[6]))
    connections.append(ConnectionFactory(
        bus_stop=stops[6], next_bus_stop=None))
    connections.append(ConnectionFactory(
        bus_stop=stops[3], next_bus_stop=stops[11]))
    connections.append(ConnectionFactory(
        bus_stop=stops[11], next_bus_stop=stops[12]))
    connections.append(ConnectionFactory(
        bus_stop=stops[12], next_bus_stop=stops[5]))

    for count in range(14, 22):
        stop = stops[count]
        if count < 21:
            next_stop = stops[count+1]
        else:
            next_stop = None
        connection = ConnectionFactory(
            bus_stop=stop, next_bus_stop=next_stop)
        connections.append(connection)

    request.cls.stops = stops
    request.cls.connections = connections
    request.cls.route_cases = route_cases
    request.cls.route_item_cases = route_item_cases
    yield


@pytest.mark.usefixtures('create_data_samples')
class TestRoute():
    """
    Class for testing all http methods of Route source.
    """

    def setup(self):
        self.c = Client()

    def create_test_put_request(self, case_index, route_name, busrouteitem_id_list, new_connections_id_list):
        """
        The method to create test PUT request with some complex data samples those is getting into method.
        """

        route_id = self.route_cases[case_index].id
        connection_id_list = [
            connection.id for connection in self.connections[:7]
        ]
        busrouteitem_id_list = [
            route_item.id for route_item in self.route_item_cases[case_index][:7]
        ]
        new_route_items_connections_list = []

        for item in new_connections_id_list:
            if item in connection_id_list:
                new_route_items_connections_list.append(
                    [
                        item,
                        busrouteitem_id_list[
                            connection_id_list.index(item)
                        ]
                    ])
            else:
                new_route_items_connections_list.append([item, None])

        request_data = {
            'id': route_id,
            'name': route_name,
            'busrouteitem_id_set': [
                {'connection_id': item[0]}
                if not item[1]
                else {'connection_id': item[0], 'id': item[1]}
                for item in new_route_items_connections_list
            ],
        }
        response = self.c.put(f'/route/{route_id}/', data=request_data,
                              content_type='application/json')
        return response

    def test_connections(self):
        """
        Test case for connection items getting.
        """

        all_connections = list(self.connections)
        for item in all_connections:
            response = self.c.get(f'/connection/{item.id}/')
            assert response.json()['id'] == item.id
            assert response.json()['bus_stop']['id'] == item.bus_stop.id
            if item.next_bus_stop:
                assert response.json()[
                    'next_bus_stop'
                ]['id'] == item.next_bus_stop.id
            else:
                assert response.json()['next_bus_stop'] is None

    def test_route_create(self):
        """
        Test case for Route creation.
        """

        route_name = 'for_test'
        connection_id_list = [
            connection.id for connection in self.connections[3:7]
        ]
        request_data = {
            'name': route_name,
            'busrouteitem_id_set': [
                {'connection_id': connection_id}
                for connection_id in connection_id_list
            ],
        }
        response = self.c.post('/route/', data=request_data,
                               content_type='application/json')
        assert response.status_code == 201
        assert response.json()['name'] == route_name
        for connection_id, busroute_item in zip(connection_id_list, response.json()['busrouteitem_set']):
            assert connection_id == busroute_item['connection']['id']

    def test_update_case_1(self):
        """
        Test case for PUT method.
        This case is update a Route resource with deleting two initial connection items.
        """

        case_index = 0
        route_name = 'updated'
        connection_id_list = [
            connection.id for connection in self.connections[2:7]
        ]
        busrouteitem_id_list = [
            route_item.id for route_item in self.route_item_cases[0][2:7]
        ]
        response = self.create_test_put_request(case_index, route_name,
                                                busrouteitem_id_list, connection_id_list)

        assert response.status_code == 200
        assert response.json()['name'] == route_name
        for connection_id, busroute_item in zip(connection_id_list, response.json()['busrouteitem_set']):
            assert connection_id == busroute_item['connection']['id']
        for busrouteitem_id, busroute_item in zip(busrouteitem_id_list, response.json()['busrouteitem_set']):
            assert busrouteitem_id == busroute_item['id']

    def test_update_case_2(self):
        """
        Test case for PUT method.
        This case is update a Route resource with deleting two ending connection items with new last connection item creating.
        """

        case_index = 1
        route_name = 'new'
        connection_id_list = [
            connection.id for connection in self.connections[:4]
        ]
        busrouteitem_id_list = [
            route_item.id for route_item in self.route_item_cases[1][:4]
        ]
        new_connection_list = connection_id_list
        new_connection_list.append(self.connections[7].id)

        response = self.create_test_put_request(case_index, route_name,
                                                busrouteitem_id_list, new_connection_list)

        assert response.status_code == 200
        assert response.json()['name'] == route_name
        for connection_id, busroute_item in zip(new_connection_list, response.json()['busrouteitem_set']):
            assert connection_id == busroute_item['connection']['id']
        for busrouteitem_id, busroute_item in zip(busrouteitem_id_list, response.json()['busrouteitem_set']):
            assert busrouteitem_id == busroute_item['id']

    def test_update_case_3(self):
        """
        Test case for PUT method.
        This case is update a Route resource with deleting first initial item,
        changing items in the middle of Route resource and changing a last items with new items.
        """

        case_index = 2
        route_name = 'to_edit'
        busrouteitem_id_list = [
            route_item.id for route_item in self.route_item_cases[2][:7]
        ]
        new_connections_id_list = [
            self.connections[1].id, self.connections[8].id,
            self.connections[4].id, self.connections[9].id,
            self.connections[10].id,
        ]

        response = self.create_test_put_request(case_index, route_name,
                                                busrouteitem_id_list, new_connections_id_list)

        assert response.status_code == 200
        assert response.json()['name'] == route_name
        response_connection_id_list = [item['connection']['id'] for item in response.json()[
            'busrouteitem_set']]
        assert all(
            item in response_connection_id_list for item in new_connections_id_list) is True

    def test_update_case_4(self):
        """
        Test case for PUT method.
        This case is update a Route resource with changing items in the middle of Route resource, changing a last items with new items.
        The main goal of this test case is to update Route to make it with more connection items.
        """

        case_index = 3
        route_name = 'edit'
        connection_id_list = [
            connection.id for connection in self.connections[:7]
        ]
        busrouteitem_id_list = [
            route_item.id for route_item in self.route_item_cases[3][:7]
        ]
        new_connections_id_list = [
            self.connections[0].id, self.connections[1].id,
            self.connections[2].id, self.connections[11].id,
            self.connections[12].id, self.connections[13].id,
            self.connections[5].id, self.connections[10].id,
        ]

        response = self.create_test_put_request(case_index, route_name,
                                                busrouteitem_id_list, new_connections_id_list)

        assert response.status_code == 200
        assert response.json()['name'] == route_name
        response_connection_id_list = [item['connection']['id'] for item in response.json()[
            'busrouteitem_set']]
        assert all(
            item in response_connection_id_list for item in new_connections_id_list) is True

    def test_update_case_5(self):
        """
        Test case for PUT method.
        This case is update a Route resource with absolutely new Connection items.
        """

        case_index = 4
        route_name = 'edit'
        connection_id_list = [
            connection.id for connection in self.connections[:7]
        ]
        busrouteitem_id_list = [
            route_item.id for route_item in self.route_item_cases[4][:7]
        ]
        new_connections_id_list = [
            self.connections[14].id, self.connections[15].id,
            self.connections[16].id, self.connections[17].id,
            self.connections[18].id, self.connections[19].id,
            self.connections[20].id, self.connections[21].id,
        ]

        response = self.create_test_put_request(case_index, route_name,
                                                busrouteitem_id_list, new_connections_id_list)

        assert response.status_code == 200
        assert response.json()['name'] == route_name
        response_connection_id_list = [item['connection']['id'] for item in response.json()[
            'busrouteitem_set']]
        assert all(
            item in response_connection_id_list for item in new_connections_id_list) is True

    def test_delete(self):
        """
        Test case for DELETE method.
        """

        response = self.c.delete(f'/route/{self.route_cases[0].id}/')
        assert response.status_code == 204
