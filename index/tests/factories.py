import factory
import faker
import factory.fuzzy
from index.models import BusRouteItem, BusStop, Connection, Route

fake = faker.Factory()


class RouteFactory(factory.DjangoModelFactory):

    class Meta:
        model = Route

    name = factory.Faker('building_number')


class BusStopFactory(factory.DjangoModelFactory):

    class Meta:
        model = BusStop

    name = factory.Faker('street_name')


class ConnectionFactory(factory.DjangoModelFactory):

    class Meta:
        model = Connection

    distance_length = factory.fuzzy.FuzzyFloat(0.1, 5)
    bus_stop = factory.SubFactory(BusStopFactory)
    next_bus_stop = factory.SubFactory(BusStopFactory)


class BusRouteItemFactory(factory.DjangoModelFactory):

    class Meta:
        model = BusRouteItem

    route = factory.SubFactory(RouteFactory)
    connection = factory.SubFactory(ConnectionFactory)
